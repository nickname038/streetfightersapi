const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
    try {
        res.data = FighterService.searchAll();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

}, responseMiddleware);

router.get('/:id', (req, res, next) => {

    try {
        const id = req.params['id'];
        res.data = FighterService.search({ id: id });
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
    if (!res.err) {
        const fighter = req.body;
        try {
            res.data = FighterService.create(fighter);
        } catch (err) {
            res.err = err;
        }
    }
    next();

}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    if (res.err) {
        try {
            const id = req.params['id'];
            const datatoUpdate = req.body;
            res.data = FighterService.update(id, datatoUpdate);
        } catch (err) {
            res.err = err;
        }
    }
    next();

}, responseMiddleware);

router.delete('/:id', (req, res, next) => {

    try {
        const id = req.params['id'];
        res.data = FighterService.delete(id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }

}, responseMiddleware);

module.exports = router;