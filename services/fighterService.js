const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    searchAll() {
        const items = FighterRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(data) {
        const fighters = this.searchAll();
        const names = fighters.map(fighter => fighter.name.toLowerCase());
        if (names.includes(data.name.toLowerCase())) {
            throw new Error('Fighter with current name has already exist');
        }
        const item = FighterRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        if (dataToUpdate.name) {
            const fighters = this.searchAll();
            const names = fighters.map(fighter => fighter.name.toLowerCase());
            if (names.includes(dataToUpdate.name.toLowerCase())) {
                throw new Error('Fighter with current name has already exist');
            }
        }

        if (!this.search({ id: id })) return null;

        const item = FighterRepository.update(id, dataToUpdate);
        if (!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const items = FighterRepository.delete(id);
        if (!items.length) {
            return null;
        }
        return items;
    }
}

module.exports = new FighterService();