const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    searchAll() {
        const items = UserRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(data) {
        const users = this.searchAll();
        const emails = users.map(user => user.email.toLowerCase());
        const phoneNumbers = users.map(user => user.phoneNumber);
        if (emails.includes(data.email.toLowerCase())) {
            throw new Error('User with current email has already exist');
        }
        if (phoneNumbers.includes(data.phoneNumber)) {
            throw new Error('User with current phone number already exist');
        }
        const item = UserRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {

        if (dataToUpdate.email || dataToUpdate.phoneNumber) {
            const users = this.searchAll();
            const emails = users.map(user => user.email.toLowerCase());
            const phoneNumbers = users.map(user => user.phoneNumber);
            if (dataToUpdate.email && emails.includes(dataToUpdate.email.toLowerCase())) {
                throw new Error('User with current email has already exist');
            }
            if (dataToUpdate.phoneNumber && phoneNumbers.includes(dataToUpdate.phoneNumber)) {
                throw new Error('User with current phone number already exist');
            }
        }

        if (!this.search({ id: id })) return null;

        const item = UserRepository.update(id, dataToUpdate);

        if (!item) {
            return null;
        }

        return item;
    }

    delete(id) {
        const items = UserRepository.delete(id);
        if (!items.length) {
            return null;
        }
        return items;
    }
}

module.exports = new UserService();