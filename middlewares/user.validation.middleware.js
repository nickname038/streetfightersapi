const { user } = require('../models/user');
const { isCreateDataValid, isUpdateDataValid } = require('./validateFunctions');

const allowedFields = Object.keys(user).filter(field => !(field === 'id'));

const isEmailValid = (email) => {
    const template = /@gmail.com$/;
    return template.test(email);
};

const isPhoneNumberValid = (phoneNumber) => {
    const template = /^\+380\d{9}/;
    return template.test(phoneNumber);
};

const isPasswordValid = (password) => {
    return password.length >= 3;
};

const validationFunctions = {
    email: isEmailValid,
    password: isPasswordValid,
    phoneNumber: isPhoneNumberValid,
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if (!isCreateDataValid(req.body, validationFunctions, allowedFields, allowedFields)) {
        res.err = new Error('Field value to create isn\'t valid');
    }
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (!isUpdateDataValid(req.body, validationFunctions, allowedFields)) {
        res.err = new Error('Field value to update isn\'t valid');
    }
    next()
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;