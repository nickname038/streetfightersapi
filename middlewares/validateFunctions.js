const isCreateDataValid = (data, validationFunctions, allowedFields, requiredFields) => {
    if (!isRequiredFildsExist(data, requiredFields)) return false;
    if (!isNotHasExcessFields(data, allowedFields)) return false;
    
    const validateField = Object.keys(data).filter(field => {
        return  Object.keys(validationFunctions).includes(field);
    });

    return validateField.every(field => {
        return validationFunctions[field](data[field]);
    });
};

const isUpdateDataValid = (data, validationFunctions, allowedFields) => {
    if (!isUpdateDataExist(data)) return false;
    if (!isNotHasExcessFields(data, allowedFields)) return false;

    const validateField = Object.keys(data).filter(field => {
        return  Object.keys(validationFunctions).includes(field);
    });

    return validateField.every(field => {
        return validationFunctions[field](data[field]);
    });
}

const isRequiredFildsExist = (objectFields, requiredFields) => {
    return requiredFields.every(field => {
        return Object.keys(objectFields).includes(field);
    });
};

const isNotHasExcessFields = (objectFields, allowedFields) => {
    return Object.keys(objectFields).every(field => {
        return allowedFields.includes(field);
    });
};

const isUpdateDataExist = (dataObj) => {
    return Object.keys(dataObj).length;
};

exports.isCreateDataValid = isCreateDataValid;
exports.isUpdateDataValid = isUpdateDataValid;
