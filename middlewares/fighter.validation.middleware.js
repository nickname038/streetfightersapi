const { fighter } = require('../models/fighter');
const { isCreateDataValid, isUpdateDataValid, } = require('./validateFunctions');

const allowedFields = Object.keys(fighter).filter(field => !(field === 'id'));

//const allowedFields = ['name', 'health', 'power', 'defense'];
const requiredFields = ['name', 'power', 'defense'];

const isNumber = n => !isNaN(parseFloat(n)) && isFinite(n);

const validationFunctions = {
    health: health => {
        return isNumber(health) &&
            parseInt(health) > 80 && parseInt(health) < 120;
    },
    power: power => {
        return isNumber(power) &&
            parseInt(power) > 1 && parseInt(power) < 100;
    },
    defense: defense => {
        return isNumber(defense) &&
            parseInt(defense) > 1 && parseInt(defense) < 10;
    },
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if (!isCreateDataValid(req.body, validationFunctions, allowedFields, requiredFields)) {
        res.err = new Error('Field value to update isn\'t valid');
    }
    if (!req.body.health) {
        req.body.health = fighter.health;
    } else {
        req.body.health = parseInt(req.body.health);
    }
    req.body.power = parseInt(req.body.power);
    req.body.defense = parseInt(req.body.defense);
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (!isUpdateDataValid(req.body, validationFunctions, allowedFields)) {
        res.err = new Error('Field value to update isn\'t valid');
    }
    if (req.body.health) {
        req.body.health = parseInt(req.body.health);
    }
    if (req.body.power) {
        req.body.power = parseInt(req.body.power);
    }
    if (req.body.defense) {
        req.body.defense = parseInt(req.body.defense);
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;